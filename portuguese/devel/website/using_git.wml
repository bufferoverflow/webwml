#use wml::debian::template title="Usando o git para manipular o código-fonte do site web"
#use wml::debian::translation-check translation="7d4921b13ecaf05ffd68527f4276d1f53cad5e16"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#work-on-repository">Trabalhando no repositório Git</a></li>
<li><a href="#write-access">Acesso de escrita ao repositório Git</a></li>
<li><a href="#notifications">Obtendo notificações</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>O <a href="https://git-scm.com/">Git</a> é um <a href="https://en.wikipedia.org/wiki/Version_control">sistema de controle de versão</a>
que ajuda a coordenar o trabalho entre vários(as) desenvolvedores(as). Cada
usuário(a) pode manter uma cópia local de um repositório main. As cópias
locais podem estar na mesma máquina ou em todo o mundo.
Os(As) desenvolvedores(as) podem então modificar a cópia local e fazer o commit
de suas alterações de volta no repositório principal quando estiverem prontos.</p>
</aside>

<h2><a id="work-on-repository">Trabalhando no repositório Git</a></h2>

<p>
Vamos direto ao assunto — nesta seção você aprenderá como criar uma cópia local
do repositório main, como manter esse repositório atualizado e como enviar seu
trabalho.
Também explicaremos como trabalhar nas traduções.
</p>

<h3><a name="get-local-repo-copy">Obtenha uma cópia local</a></h3>

<p>
Primeiro, instale o Git. Em seguida, configure o Git e insira seu nome e
endereço de e-mail.
Se você é um(a) usuário(a) novo(a) do Git, provavelmente é uma boa ideia
primeiro ler a documentação geral do Git.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://git-scm.com/doc">Documentação do Git</a></button></p>

<p>
Seu próximo passo é clonar o repositório (em outras palavras: fazer uma cópia
local dele).
Existem duas maneiras para fazer isso:
</p> 

<ul>
  <li>Registre uma conta em <url https://salsa.debian.org/> e habilite o acesso
  SSH fazendo o upload de uma chave SSH pública para sua conta no Salsa. Veja
  as <a href="https://salsa.debian.org/help/ssh/README.md">páginas de ajuda do Salsa</a>
  para mais detalhes. Então você pode clonar o repositório <code>webwml</code>
  usando o seguinte comando:
<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git
</pre>
  </li>
  <li>Como alternativa, você pode clonar o repositório usando o protocolo HTTPS.
  Lembre-se de que isso criará o repositório local, mas você não poderá enviar
  as alterações diretamente dessa maneira:
<pre>
  git clone https://salsa.debian.org/webmaster-team/webwml.git
</pre>
   </li>
</ul> 

<p>
<strong>Dica:</strong> clonar todo o repositório <code>webwml</code> requer o
download de aproximadamente 1,3 GB de dados, o que pode ser bastante se você
estiver em uma conexão de Internet lenta ou instável. Portanto, é possível
definir uma profundidade mínima para um download inicial menor:
</p> 

<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git --depth 1
</pre>

<p>Depois de obter um repositório utilizável (raso), você pode aprofundar a
cópia local e, eventualmente, convertê-la em um repositório local completo:</p>

<pre>
  git fetch --deepen=1000 # aprofunda o repo para mais 1000 commits
  git fetch --unshallow   # busca todos os commits ausentes, converte o repositório para um completo
</pre>

<p>Você também pode obter apenas um subconjunto das páginas:</p>

<ol>
  <li><code>git clone --no-checkout git@salsa.debian.org:webmaster-team/webwml.git</code></li>
  <li><code>cd webwml</code></li>
  <li><code>git config core.sparseCheckout true</code></li>
  <li>Crie o arquivo <code>.git/info/sparse-checkout</code> dentro do diretório
  <code>webwml</code> para definir o conteúdo que você deseja obter. Por
  exemplo, se deseja recuperar apenas os arquivos base, traduções do
  inglês, catalão e espanhol, o arquivo será dessa forma:
    <pre>
      /*
      !/[a-z]*/
      /english/
      /catalan/
      /spanish/
    </pre></li>
  <li>A seguir, poderá obter o repo: <code>git checkout --</code></li>
</ol>

<h3><a name="submit-changes">Envie as mudanças locais</a></h3>

<h4><a name="keep-local-repo-up-to-date">Mantenha seu repo local atualizado</a></h4>

<p>A cada poucos dias (e definitivamente antes de começar algum trabalho de
edição!) você deve fazer um</p>

<pre>
  git pull
</pre>

<p>para obter quaisquer arquivos do repositório que foram alterados.</p>

<p>
É altamente recomendável manter seu diretório de trabalho Git local limpo antes
de executar <code>git pull</code> e começar a editar alguns arquivos.
Se você tiver alterações que não foram feitos commit, ou commits locais que não
estão presentes no repositório remoto no branch atual, executar
<code>git pull</code> criará automaticamente merge commits ou até mesmo falhará
devido a conflitos. Por favor, considere manter seu trabalho inacabado em
outro branch ou usar comandos como <code>git stash</code>.
</p>

<p>Nota: O Git é um sistema de controle de versão distribuído (não
centralizado). Isso significa que quando você fizer o commit das alterações,
elas serão armazenadas apenas em seu repositório local. Para compartilhá-los
com outras pessoas, você também precisará enviar suas alterações para o
repositório central no Salsa.</p>

<h4><a name="example-edit-english-file">Exemplo: editando alguns arquivos</a></h4>

<p>
Vejamos um exemplo mais prático e uma sessão de edição típica.
Estamos assumindo que você obteve uma
<a href="#get-local-repo-copy">cópia local</a> do repositório usando
<code>git clone</code>. Seus próximos passos são:
</p>

<ol>
  <li><code>git pull</code></li>
  <li>Agora você pode começar a editar e fazer alterações nos arquivos.</li>
  <li>Quando terminar, faça o commit de suas alterações em seu repositório
  local:
    <pre>
    git add caminho/para/os/arquivos
    git commit -m "Sua mensagem de commit"
    </pre></li>
  <li>Se você tiver <a href="#write-access">acesso ilimitado de escrita</a> no
  repositório remoto <code>webwml</code>, poderá enviar suas alterações
  diretamente para o repositório no Salsa: <code>git push</code></li>
  <li>Se não tiver acesso direto de escrita ao repositório
  <code>webwml</code>, envie suas alterações com um
  <a href="#write-access-via-merge-request">merge request</a> ou entre em
  contato com outros(as) desenvolvedores(as) para obter ajuda.</li>
</ol>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://git-scm.com/docs/gittutorial">Documentação do Git</a></button></p>

<h4><a name="closing-debian-bug-in-git-commits">Fechando bugs do Debian Bugs com commits no Git</a></h4>

<p>
Se você incluir <code>Closes: #</code><var>nnnnnn</var> no conteúdo do log do
seu commit, então o bug de número <code>#</code><var>nnnnnn</var> será fechado
automaticamente quando você enviar suas alterações. A forma precisa disso é a
mesma que
<a href="$(DOC)/debian-policy/ch-source.html#id24">na política Debian</a>.</p>

<h4><a name="links-using-http-https">Links com HTTP/HTTPS</a></h4>

<p>Muitos sites web do Debian suportam SSL/TLS, por favor use links com
HTTPS sempre que possível e sensato. <strong>No entanto</strong>, alguns sites
web Debian/DebConf/SPI/etc não têm suporte a HTTPS ou usam apenas o CA SPI (e
não um CA SSL confiável para todos os navegadores). Para evitar causar
mensagens de erro para usuários(as) não Debian, por favor não crie links para
esses sites usando HTTPS.</p>

<p>O repositório Git rejeitará commits contendo links HTTP simples para sites
web do Debian que suportam HTTPS, ou contendo links HTTPS para sites web
Debian/DebConf/SPI que são conhecidos por não suportarem HTTPS ou usarem
certificados assinados apenas pela SPI.</p>

<h3><a name="translation-work">Trabalhando as traduções</a></h3>

<p>As traduções devem sempre ser mantidas atualizadas com o arquivo em inglês
correspondente. O cabeçalho <code>translation-check</code> nos arquivos de
tradução é usado para rastrear em qual versão do arquivo em inglês a tradução
atual foi baseada. Se você alterar os arquivos traduzidos, precisará atualizar
o cabeçalho <code>translation-check</code> para corresponder ao hash do commit
Git da alteração correspondente no arquivo em inglês. Você pode identificar o
hash com o seguinte comando:</p> 

<pre>
  git log caminho/para/arquivo/em/inglês
</pre>

<p>Se você fizer uma nova tradução de um arquivo, por favor use o script
<code>copypage.pl</code>.
Ele cria um template para seu idioma, incluindo o cabeçalho de tradução
correto.</p>

<h4><a name="translation-smart-change">Alterações de tradução com smart_change.pl</a></h4>

<p><code>smart_change.pl</code> é um script projetado para tornar mais fácil
atualizar arquivos originais e suas traduções juntos. Existem duas maneiras de
usá-lo, dependendo das alterações que você está fazendo.</p>

<p>
Veja como usar <code>smart_change.pl</code> e como atualizar os cabeçalhos
<code>translation-check</code> quando estiver trabalhando em arquivos
manualmente:
</p>

<ol>
  <li>Faça as alterações nos arquivos originais e faça o commit das suas
  alterações.</li>
  <li>Atualize as traduções.</li>
  <li>Execute <code>smart_change.pl -c COMMIT_HASH</code> (use o hash do commit
  das alterações nos arquivos originais).
  Ele pegará as alterações e atualizará os cabeçalhos dos arquivos
  traduzidos.</li>
  <li>Revise as alterações (por exemplo, com <code>git diff</code>).</li>
  <li>Faça o commit das alterações de tradução.</li>
</ol>

<p>
Como alternativa, você pode trabalhar com expressões regulares para fazer
várias alterações nos arquivos em uma única passagem:
</p>

<ol>
  <li>Execute <code>smart_change.pl -s s/FOO/BAR/ origfile1 origfile2 ...</code></li>
  <li>Revise as alterações (por exemplo, com <code>git diff</code>).</li>
  <li>Faça o commit dos arquivos originais.</li>
  <li>Execute <code>smart_change.pl origfile1 origfile2</code>
    (ou seja, <strong>sem o regexp</strong> desta vez). Ele agora apenas
    atualizará os cabeçalhos dos arquivos traduzidos.</li>
  <li>Por fim, faça o commit das alterações de tradução.</li>
</ol>

<p>
É certo que isso requer um pouco mais de esforço do que o primeiro exemplo,
pois envolve dois commits, mas é inevitável devido à maneira como os hashes do
Git funcionam.
</p>

<h2><a id="write-access">Acesso de escrita ao repositório Git</a></h2>

<p>
O código-fonte do site web do Debian é gerenciado com o Git e localizado em
<url https://salsa.debian.org/webmaster-team/webwml/>.
Por padrão, convidados(as) não têm permissão para enviar os commits para o
repositório de código-fonte.
Se você deseja contribuir com o site web do Debian, precisa de algum tipo
de permissão para obter acesso de escrita no repositório.
</p>

<h3><a name="write-access-unlimited">Acesso ilimitado de escrita</a></h3>

<p>
Se você precisa de acesso ilimitado de escrita no repositório, por exemplo, se
está prestes a se tornar um(a) contribuidor(a) frequente, por favor,
solicite o acesso de escrita através da interface web
<url https://salsa.debian.org/webmaster-team/webwml/> após fazer login na
plataforma do Salsa do Debian.
</p>

<p>
Se você é novo(a) no desenvolvimento do site web do Debian e não tem
experiência anterior, por favor envie um e-mail para
<a href="mailto:debian-www@lists.debian.org">debian-www@lists.debian.org</a> e
se apresente antes de solicitar acesso ilimitado de escrita. Seja gentil
e nos conte mais sobre você, por exemplo, em qual parte do site web
planeja trabalhar, quais idiomas fala, e também se há outro(a)
membro(a) da equipe do Debian que pode atestar por você.
</p> 

<h3><a name="write-access-via-merge-request">Merge Requests</a></h3>

<p>
Não é necessário obter acesso ilimitado de escrita no repositório — você sempre
pode enviar um merge request e permitir que outros(as) desenvolvedores(as)
revisem e aceitem seu trabalho. Por favor, siga o procedimento padrão para
solicitações de merge requests fornecido pela plataforma Salsa GitLab por meio
de sua interface web e leia os dois documentos a seguir:
</p>

<ul>
  <li><a href="https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html">Fluxo de trabalho do fork do projeto</a></li>
  <li><a href="https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork">Quando você trabalha em um fork</a></li>
</ul>

<p>
Por favor observe que os merge requests não são monitoradas por todos(as) os(as)
desenvolvedores(as) do site web. Portanto, pode levar algum tempo até que
receba algum feedback. Se quer saber se sua contribuição será aceita ou
não, por favor envie um e-mail para a lista de discussão
<a href="https://lists.debian.org/debian-www/">debian-www</a>
e peça uma revisão.
</p>

<h2><a id="notifications">Obtendo notificações</a></h2>

<p>
Se você está trabalhando no site web do Debian, provavelmente quer saber o
que está acontecendo no repositório <code>webwml</code>. Há duas maneiras de se
manter atualizado(a): notificações de commit e notificações de merge request.
</p>

<h3><a name="commit-notifications">Recebendo notificações de commit</a></h3>

<p>Configuramos o projeto <code>webwml</code> no Salsa para que os commits
sejam mostrados no canal IRC #debian-www.</p>

<p>
Se deseja receber notificações sobre commits no repo <code>webwml</code>
por e-mail, assine o pseudopacote <code>www.debian.org</code> via
tracker.debian.org e ative a palavra-chave <code>vcs</code>, seguindo estes
passos (apenas uma vez):</p>

<ol>
  <li>Abra um navegador web e vá para <url https://tracker.debian.org/pkg/www.debian.org>.</li>
  <li>Assine o pseudopacote <code>www.debian.org</code> (você pode autenticar
      via SSO ou registrar um e-mail e senha, se ainda não estiver usando
      o tracker.debian.org para outros propósitos).</li>
  <li>Vá para <url https://tracker.debian.org/accounts/subscriptions/>, depois
      em <code>>modify keywords</code>, marque <code>vcs</code> (se não estiver
      marcado) e salve.</li>
  <li>A partir de agora, você receberá e-mails quando alguém fizer commit no
      repositório <code>webwml</code>.</li>
</ol>

<h3><a name="merge-request-notifications">Recebendo notificações de Merge Request</a></h3>

<p>
Se deseja receber e-mails de notificação sempre que houver novos merge
requests enviadas para o repositório <code>webwml</code> no Salsa, você pode
configurar suas opções de notificação na interface web seguindo estas etapas:
</p>

<ol>
  <li>Faça login na sua conta do Salsa e vá para a página do projeto.</li>
  <li>Clique no ícone do sino na parte superior da página inicial do
      projeto.</li>
  <li>Selecione o nível de notificação de sua preferência.</li>
</ol>
