#use wml::debian::translation-check translation="18c53e0c90d9bed70c3d0aa8b74b0b346e23c3c3" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Résolution générale : Modification de la procédure de résolution</define-tag>
<define-tag status>F</define-tag>
# meanings of the <status> tag:
# P: proposed
# D: discussed
# V: voted on
# F: finished
# O: other (or just write anything else)

#use wml::debian::template title="<pagetitle>" BARETITLE="true" NOHEADER="true"
#use wml::debian::toc
#use wml::debian::votebar


    <h1><pagetitle></h1>
    <toc-display />

# The Tags beginning with v are will become H3 headings and are defined in
# english/template/debian/votebar.wml
# all possible Tags:

# vdate, vtimeline, vnominations, vdebate, vplatforms,
# Proposers
#          vproposer,  vproposera, vproposerb, vproposerc, vproposerd,
#          vproposere, vproposerf
# Seconds
#          vseconds,   vsecondsa, vsecondsb, vsecondsc, vsecondsd, vsecondse,
#          vsecondsf,  vopposition
# vtext, vtextb, vtextc, vtextd, vtexte, vtextf
# vchoices
# vamendments, vamendmentproposer, vamendmentseconds, vamendmenttext
# vproceedings, vmajorityreq, vstatistics, vquorum, vmindiscuss,
# vballot, vforum, voutcome


    <vtimeline />
    <table class="vote">
      <tr>
        <th>Proposition et amendement ;</th>
        <td>20 novembre 2021</td>
		<td></td>
      </tr>
      <tr>
        <th>Période de débat :</th>
		<td>16 décembre 2021</td>
		<td></td>
      </tr>
          <tr>
            <th>Période de vote :</th>
            <td>Samedi 15 janvier 2022 00:00:00 UTC</td>
            <td>Vendredi 28 janvier 2022 23:59:59 UTC</td>
    </table>

    <vproposer />
    <p>Russ Allbery [<email rra@debian.org>]
	[<a href='https://lists.debian.org/debian-vote/2021/11/msg00107.html'>texte de la proposition</a>]
	[<a href='https://lists.debian.org/debian-vote/2021/11/msg00145.html'>amendement</a>]
	[<a href='https://lists.debian.org/debian-vote/2021/12/msg00017.html'>amendement</a>]
    </p>
    <vseconds />
    <ol>
	<li>Timo Röhling [<email roehling@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/11/msg00108.html'>message</a>] </li>
	<li>Philip Hands [<email philh@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/11/msg00109.html'>message</a>] </li>
	<li>Sam Hartman [<email hartmans@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/11/msg00111.html'>message</a>] </li>
	<li>Pierre-Elliott Bécue [<email peb@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/11/msg00112.html'>message</a>] </li>
	<li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/11/msg00115.html'>message</a>] </li>
	<li>Gunnar Wolf [<email gwolf@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/11/msg00154.html'>message</a>] </li>
	<li>Sean Whitton [<email spwhitton@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/11/msg00165.html'>message</a>] </li>
    </ol>
    <vtext />
	<h3>Choix 1 : Amender le processus de vote et fixer une période maximale de débat</h3>

<pre>
Raison
======

Nous avons découvert plusieurs problèmes dans le mécanisme constitutionnel
actuel pour la préparation des résolutions du Comité Technique ou des
Résolutions Générales en vue du vote :

* Le moment pour l'appel à voter est discrétionnaire et pourrait être
  utilisé de façon stratégique pour interrompre la discussion alors que
  d'autres membres sont en train de préparer des options de vote
  supplémentaires.
* La personne qui a l'initiative de la proposition d'une résolution
  générale a un contrôle spécial sur le moment choisi pour le vote qui
  pourrait être utilisé de façon stratégique pour désavantager les autres
  options de vote.
* La description de la procédure pour ajouter et gérer les options de vote
  sont difficiles à comprendre.
* Le choix actuel par défaut de « poursuivre le débat » dans le cadre d'une
  résolution générale a des implications qui vont plus loin que le rejet
  des autres options, ce qui peut, à l'encontre de son intention,
  décourager les développeurs de le placer au-dessus des options qu'ils
  souhaitent rejeter.

Les implications réelles ou potentielles de ces problèmes ont provoqué un
problème lors du vote du comité technique sur systemd et sur les
résolutions générales 2019-002 et 2021-002, ce qui a rendu plus difficile
l'obtention par le projet d'un résultat juste et largement respecté.

Cette modification de la constitution tente de corriger ces problèmes en

* séparant la procédure du comité technique de celle de la résolution
  générale dans la mesure où elles répondent à des besoins différents ;
* réclamant un consensus (passif) parmi les membres du CT sur le fait
  qu'une résolution est prête à être soumise au vote ;
* fixant une période maximale de discussion pour une résolution du CT et
  ensuite déclencher le vote ;
* fixant une période maximale de discussion pour une résolution générale
  de sorte que le moment du vote soit prévisible ;
* prolongeant automatiquement la discussion sur une résolution générale si
  les options de vote changent ;
* modifiant la procédure de résolution générale pour traiter toutes les
  options de vote de façon équitable avec une procédure plus claire pour
  les ajouts, les retraits et les amendements ;
* modifiant l'option par défaut d'une résolution générale « None Of The
  Above » (Aucune de celles qui précèdent) et en
* clarifiant le pouvoir discrétionnaire du Secrétaire du Projet.

Elle corrige également un problème technique qui laisse le résultat d'un
vote pour la présidence du comité technique indéterminé en cas de partage
des voix et clarifie les responsabilités du CT pour introduire une
résolution générale comme prévu au §4.2.1.

Effet de la résolution générale
===============================

Les développeurs Debian, par résolution générale, amendent la constitution
de Debian selon le §4.1.2 comme suit. Cette résolution générale a
besoin d'une majorité qualifiée de 3 contre 1.

Section 4.2.1
-------------

Remplacer « amendement » par « option de vote »

Section 4.2.4
-------------

Retirer la phrase « La période de discussion minimale est de 2 semaines,
mais elle peut être modifiée d'au plus une semaine par le responsable du
projet. » (Une version modifiée de cette clause est ajoutée à §A plus bas.)
Ajouter à la fin de ce paragraphe :

    L'option par défaut est « None of the above » (Aucune de celles qui
    précèdent).

Section 4.2.5
-------------

Remplacer « amendements » par « options de vote ».

Section 5.1.5
-------------

La remplacer complètement par :

    Proposition de résolutions générales et d'options de vote pour les
    résolutions générales. Quand elles sont proposées par le responsable
    du projet, les résolutions générales et les options de vote n'ont
    pas besoin de soutiens ; voir §4.2.1.

Section 5.2.7
-------------

Remplacer la « §A.6  par la « §A.5 ».

Section 6.1.7
-------------

Remplacer la « §A.6  par la « §A.5 ».

Ajouter à la fin de cette section :

    Il n'y a pas de voix discriminante. S'il y a plusieurs options qui ne
    sont pas battues dans l'ensemble de Schwartz à la fin de §A.5.8, le
    vainqueur sera choisi au hasard parmi ces options, au moyen d'un
    mécanisme choisi par le secrétaire du projet.

Section 6.3
-----------

Remplacer 6.3.1 dans sa totalité par :

    1. Procédure de résolution.

       Le comité technique utilise la procédure suivante pour préparer une
       résolution pour le vote :

       1. N'importe quel membre du comité technique peut proposer une
          résolution. Cela crée un vote initial à deux options, la seconde
          option étant par défaut « Aucune de celles qui précèdent ». le
          déposant de la résolution devient le déposant de l'option de
          vote.

       2. N'importe quel membre du comité technique peut proposer une
          option de vote supplémentaire ou modifier ou retirer l'option de
          vote qu'il a proposé.

       3. Si toutes les options de vote, sauf l'option par défaut, sont
          retirées, la procédure est supprimée.

       4. N'importe quel membre du comité technique peut appeler à un
          scrutin sur l'option de vote dans son libellé actuel. Ce vote
          débute immédiatement, mais si un autre membre du comité technique
          s'oppose à l'appel à voter avant que le vote ne soit achevé, le
          vote est annulé et reste sans effet.

       5. Deux semaines après la proposition originale les options de vote
          ne peuvent plus être modifiées et le vote débute immédiatement.
          Ce scrutin ne peut plus être annulé.

       6. Si un scrutin est annulé en vertu du §6.3.1.4 plus de 13 jours
          après le dépôt initial de la proposition, un vote tel que
          spécifié au §6.3.1.5 débute à la place 24 heures après
          le moment de l'annulation. Durant cette période de 24 heures,
          personne ne peut appeler à un vote, mais les membres du comité
          technique peuvent effectuer des modifications du vote selon le
          §6.3.1.2.

Ajout d'un nouveau paragraphe au début du 6.3.2 suivant « Détails
concernant le vote » :

       Le résultat des votes est obtenu par le mécanisme de décompte des
       votes dans le §A.5. La période de vote dure une semaine ou
       jusqu'à ce que le résultat ne fasse plus aucun doute, présumant
       qu'aucun membre ne va modifier son vote, selon le délai le plus
       court. Les membres peuvent modifier leur vote jusqu'à la fin de la
       période de vote. Le quorum est de deux. Le ou la présidente a une
       voix discriminante. L'option par défaut est « Aucune de celles qui
       précèdent ».

Retirer « Le ou la présidente a une voix discriminante » du texte existant
et transformer le reste du texte en un second paragraphe séparé.

Dans 6.3.3, remplacer « amendements » par « options de vote ».

Ajouter, à la fin de la section 6.3, la nouvelle section suivante :

    7. Proposition d'une résolution générale.

       Quand le comité technique propose une résolution générale ou une
       option de vote dans une résolution générale au projet en vertu du
       §4.2.1, il peut déléguer l'autorité pour retirer, amender ou faire
       des modifications mineures à l'option de vote à un de ses membres.
       S'il ne le fait pas, ces décisions doivent être prises par une
       résolution du comité technique.

Section A
---------

Remplacer les §A.0 à §A.4 en leur totalité par :

    A.0. Proposition

    1. La procédure formelle commence lorsqu'un projet de résolution est
       proposé et soutenu, comme spécifié dans le §4.2.1.

    2. Ce projet de résolution devient une option de vote dans un vote
       initial à deux options, l'autre option étant l'option par défaut et
       le déposant du projet de résolution devient le déposant de cette
       option de vote.

    A.1. Discussion et amendement

    1. La période de discussion débute lorsqu'une proposition de résolution
       est soumise et soutenue. La période de discussion minimale est de
       deux semaines. La période de discussion maximale est de trois
       semaines.

    2. Une nouvelle option de vote peut être proposée et soutenue
       conformément aux exigences d'une nouvelle résolution.

    3. Le déposant d'une option de vote peut amender cette option à
       condition qu'aucun des parrains de cette option de vote, au moment
       où l'amendement est proposé, ne soit en désaccord avec cette
       modification dans les 24 heures. Si l'un d'entre eux s'y oppose,
       l'option de vote demeure inchangée.

    4. L'ajout d'une option de vote ou la modification d'une option de vote
       au moyen d'un amendement changent la fin de la période de discussion
       pour qu'elle survienne une semaine après cette action, à moins que
       cela ne rende la durée totale de discussion plus courte que la
       période de discussion minimale ou plus longue que la durée maximale
       de discussion. Dans ce dernier cas, la durée de la période de
       discussion est plutôt fixée à la durée maximale de discussion.

    5. Le déposant d'une option de vote peut faire des modifications
       mineures à cette option (par exemple, des corrections d'erreurs
       typographiques ou d'incohérences ou d'autres modifications qui n'en
       modifient pas la signification), pourvu qu'aucun développeur ne s'y
       oppose dans les 24 heures. Dans ce cas, la durée de la période de
       discussion n'est pas changée. Si un développeur manifeste son
       désaccord, la modification doit plutôt être faite sous la forme d'un
       amendement prévu au §A.1.3.

    6. Le responsable du projet peut, à n'importe quel moment de la
       procédure, augmenter ou réduire la période maximale ou minimale de
       discussion jusqu'à une semaine par rapport à la valeur originale
       prévue au §A.1.1, si ce n'est qu'il ne peut le faire d'une façon
       telle que la période de discussion se termine dans les 48 heures
       après que la modification ait eu lieu. La durée de la période
       de discussion est alors recalculée comme si les nouvelles durées
       minimale et maximale avaient été en place durant toutes les
       modifications précédentes en vertu des §A.1.1 et §A.1.4.

    7. L'option par défaut n'a pas de déposant ni de parrain, et ne peut
       pas être amendée ou retirée.

    A.2. Retrait des options de vote

    1. Le déposant d'une option de vote peut se retirer. S'il le fait, de
       nouveaux déposants peuvent se manifester pour la garder, auquel cas
       la première personne qui le fait devient le nouveau déposant et les
       autres des parrains si ce n'est pas déjà le cas. Tout nouveau
       déposant ou parrain doit remplir les conditions pour proposer ou
       soutenir une nouvelle résolution.

    2. Le parrain d'une option de vote peut se retirer.

    3. Si le retrait du déposant ou des parrains implique qu'une option de
       vote n'a plus de déposant ou plus assez de parrains pour remplir les
       conditions d'une nouvelle résolution, et si 24 heures se passent
       sans que cela ne soit corrigé par l'engagement d'un autre déposant ou
       d'autres parrains, elle est retirée de la proposition de vote. Cela
       ne modifie pas la durée de la période de discussion.

    4. Si toutes les options de vote, à part l'option par défaut, sont
       retirées, la résolution est annulée et ne sera pas soumise au vote.

    A.3. Appel à voter

    1. Après la fin de la période de discussion, le secrétaire du projet
       publiera le vote et l'appel à voter. Le secrétaire du projet peut
       le faire immédiatement après la fin de la période de discussion et
       doit le faire dans les sept jours après la fin de la période de
       discussion.

    2. Le secrétaire du projet détermine l'ordre des options de vote et
       les résumés utilisés pour le vote. Le secrétaire du projet peut
       demander aux déposants d'une option de vote de rédiger ces résumés
       et peut les réviser pour les clarifier, à leur discrétion.

    3. Des modifications mineures des options de votes prévues au §A.1.5
       peuvent seulement être faites s'il reste au moins 24 heures dans la
       période discussion ou si le secrétaire du projet partage l'avis que 
       la modification ne change pas le sens de l'option du vote et (si ce
       pouvait être le cas) garantit de retarder le vote. Le secrétaire du
       projet accordera 24 heures pour des objections après une
       modification de ce type avant de publier l'appel à voter.

    4. Aucune nouvelle option de vote ne peut être proposée, aucune option
       de vote ne peut être amendée, et aucun déposant ou parrain ne peut
       se retirer s'il reste moins de 24 heures dans la période de
       discussion à moins que cette action n'entraîne avec succès
       l'extension d'au moins 24 heures supplémentaires de la période de
       discussion visée au point §A.1.4.

    5. Des actions pour préserver un vote existant peuvent être menées
       pendant les dernières 24 heures de la période de discussion, à
       savoir un parrain objectant à une modification mineure visée au
       §A.1.3, un développeur objectant à une modification mineure visée au
       §A.1.5, se proposant comme déposant pour une option de vote
       existante dont le déposant d'origine s'est retiré comme prévu au
       §A.2.1, ou parrainant une option de vote existante qui ne dispose
       plus du nombre requis de parrains du fait du retrait d'un parrain
       comme prévu au §A.2.2.

    6. Le secrétaire du projet peut faire une exception au §A.3.4 et
       accepter des modifications du vote après qu'elles ne sont plus
       permises, à condition que cela soit fait au moins 24 heures avant
       la publication de l'appel au vote. Toutes les autres conditions pour
       faire une modification au vote doivent encore être réunies. Cela
       devrait être rare et ne devrait être fait que si le secrétaire du
       projet pense qu'il serait préjudiciable à l'intérêt supérieur du
       projet que cette modification ne soit pas mise en œuvre.

    A.4. Procédure de vote

    1. Les options qui ne nécessitent pas de majorité qualifiée ont besoin
       d'une majorité de 1 contre 1. L'option par défaut ne doit nécessiter
       aucune majorité qualifiée.

    2. Les votes sont comptabilisés selon les règles du §A.5.

    3. En cas de doute le secrétaire du projet devra décider sur les
       questions de procédure.

Renommer le §A.6 en §A.5.

Remplacer le paragraphe à la fin du §A.6 (maintenant §A.5) par :

    Quand le mécanisme de décompte des votes de la procédure de résolution
    standard doit être utilisé, le texte qui y fait référence doit
    spécifier qui a une voix discriminante, le quorum, l'option par défaut
    et quelle majorité qualifiée est requise. L'option par défaut ne doit
    pas requérir de majorité qualifiée.
</pre>

    <vproposerb />
    <p>Wouter Verhelst [<email wouter@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/11/msg00118.html'>texte de la proposition</a>]
	[<a href='https://lists.debian.org/debian-vote/2021/11/msg00124.html'>amendement</a>]
	[<a href='https://lists.debian.org/debian-vote/2021/11/msg00140.html'>amendement</a>]
	[<a href='https://lists.debian.org/debian-vote/2021/12/msg00009.html'>amendment</a>]
    </p>
    <vsecondsb />
    <ol>
        <li>Holger Levsen [<email holger@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/11/msg00119.html'>message</a>] </li>
        <li>Pierre-Elliott Bécue [<email peb@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/11/msg00120.html'>message</a>] </li>
        <li>Mathias Behrle [<email mbehrle@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/11/msg00125.html'>message</a>] </li>
        <li>Kyle Robbertze [<email paddatrapper@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/11/msg00150.html'>message</a>] </li>
        <li>Mattia Rizzolo [<email mattia@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/11/msg00161.html'>message</a>] </li>
        <li>Louis-Philippe Véronneau [<email pollo@debian.org>] [<a href='https://lists.debian.org/debian-vote/2021/11/msg00162.html'>message</a>] </li>
    </ol>
    <vtextb />
        <h3>Choix 2 : Amender le processus de résolution, permettre de prolonger la période de débat</h3>

<pre>
Raison
======

L'essentiel des raisons de la proposition de Russ s'applique encore et,
d'ailleurs, cet amendement est basé sur elle. Néanmoins, la manière dont
les choses s'enchaînent est délibérément différente.

Notre système de vote, qu'aucune des propositions ne modifie, en tant que
mécanisme de vote Condorcet, ne souffre pas directement d'un trop grand
nombre d'options lors du scrutin. Même s'il est souhaitable de s'assurer
que le nombre d'options du vote n'est pas extrêmement élevé pour des
raisons pratiques ou de fatigue de l'électeur, il est néanmoins d'une
importance primordiale que toutes les options *pertinentes* soient
représentées dans le scrutin, de sorte que le résultat du vote n'est pas
remis en question par le simple fait qu'une option particulière n'était pas
représentée dans le scrutin. Pour que cela soit possible, il faut qu'il y
ait suffisamment de temps pour débattre de toutes les options pertinentes.

La proposition de Russ introduit une limite stricte de trois semaines pour
tout le processus du vote, en supposant que cela sera presque toujours
assez, et en comptant sur le retrait d'options et sur le redémarrage de la
procédure de vote dans le cas extrême où il s'avère que plus de temps est
nécessaire ; dans la proposition de Russ, faire ainsi augmenterait le temps
de discussion de deux semaines supplémentaires au moins (ou d'une seule si
le responsable du projet réduit le temps de discussion).

Lors de votes controversés, je crois qu'il y a moins de chance que tous les
déposants soient enclins à utiliser cette échappatoire de retrait du vote
et du redémarrage de la procédure ; et en même temps, les votes controversés
sont les plus susceptibles de nécessiter beaucoup de discussions pour bâtir
un scrutin correct, ce qui implique qu'ils devraient plus probablement
nécessiter du temps supplémentaire – pas nécessairement deux semaines de
plus – pour que le scrutin soit abouti.

En même temps, je ne suis pas indifférent aux arguments de prévisibilité, à
la loi des « rendements décroissants » et aux abus de procédure qui
semblent être les principaux arguments en faveur d'une limite stricte à
trois semaines.

Pour cette raison, ma proposition n'introduit par de limite stricte et rend
*toujours* théoriquement possible d'accroître le temps de discussion, mais
le fait d'une manière telle que le rallongement de la période de discussion
devient de plus en plus difficile avec le temps. Je crois qu'il est mieux
que la constitution permette à un groupe de personnes d'avoir un peu de
temps supplémentaire avant qu'ils puissent terminer leur proposition
d'option de vote, plutôt que d'exiger que la période de discussion
redémarre complètement au moyen d'un retrait ou d'une échappatoire. En même
temps, cette échappatoire n'est pas supprimée, bien que j'espère qu'elle
sera probablement moins utilisée.

Le mécanisme proposé fixe la durée initiale de discussion à une semaine,
mais permet qu'il soit raisonnablement facile de la prolonger à deux ou
trois semaines, rend plus difficile de l'étendre à quatre semaines et rend
très improbable (mais pas impossible) qu'elle aille au-delà.

Texte de la résolution générale
===============================

Les développeurs Debian, par résolution générale, amendent la constitution
de Debian sous le point 4.1.2 comme suit. Cette résolution générale a
besoin d'une majorité qualifiée de 3 contre 1.

Sections de 4 à 7
-----------------

Reprendre la proposition de Russ, en remplaçant les références croisées de
§A.5 à §A.6, là où c'est nécessaire.

Section A
---------

Remplacer la section A comme dans la proposition de Russ, avec les
modifications suivantes :

A.1.1. Remplacer la phrase « La période de discussion minimale est de deux
       semaines. » par « La période de discussion initiale est d'une
       semaine. » Retirer la phrase « La période de discussion maximale est
       de trois semaines ».

A.1.4. Retirer la section dans sa totalité

A.1.5. Renommer en A.1.4 et retirer la phrase « Dans ce cas, la durée de
       la période de discussion n'est pas changée. »

A.1.6. Retirer la section dans sa totalité

A.1.7. Renommer en A.1.5.

Après A.2, insérer :

A.3. Extension du temps de discussion.

1. Quand il reste moins de 48 heures du temps de discussion, tout
   développeur peut proposer une extension du temps de discussion,
   soumise aux limitations de §A.3.3. Ces extensions peuvent être soutenues
   selon les règles qui s'appliquent aux nouvelles options de vote.

2. Dès qu'une extension de temps a reçu le nombre requis de soutiens, ces
   parrainages sont verrouillés et ne peuvent pas être retirés et
   l'extension de temps est active.

3. Quand une extension de temps a reçu le nombre requis de soutiens, ses
   déposants et parrains ne peuvent plus proposer ou soutenir une nouvelle
   extension de temps pour le même scrutin et tout nouveau soutien pour la
   même proposition d'extension sera ignorée aux fins du présent
   paragraphe. En cas de doute, le secrétaire du projet décide comment est
   déterminé l'ordre des parrainages.

4. Les deux premières extensions de temps acceptées prolongeront la période
   de discussion d'une semaine ; toute extension supplémentaire prolongera
   la période de discussion de 72 heures.

5. Une fois que la période de discussion dépasse quatre semaines, tout
   développeur peut s'opposer à une nouvelle extension de temps. Les
   développeurs qui ont précédemment proposé ou soutenu une extension
   peuvent également s'opposer. Si le nombre d'oppositions dépasse celui du
   déposant et des parrains, y compris les parrains qui sont ignorés en
   vertu du paragraphe §A.3.3, l'extension de temps ne sera pas activé et la
   période de discussion n'est pas modifiée.

6. Une fois que la période de discussion est expirée, toute proposition
   d'extension de temps en attente qui n'a pas encore reçu le nombre requis
   de parrains est nulle et non avenue, et aucune extension de temps
   supplémentaire ne peut être proposée.

A.3. Renommer en A.4.

A.3.6 (maintenant A.4.6) : remplacer « A.3.4 » par « A.4.4 ».

A.4. Renommer en A.5.

A.4.2 (maintenant A.5.2) : remplacer « §A.5 » par « §A.6 ».

A.5. Renommer (à nouveau) en A.6.
</pre>

    <vquorum />
     <p>
       Avec la liste actuelle des <a href="vote_003_quorum.log">développeurs
         ayant voté</a>, nous avons :
     </p>
    <pre>
#include 'vote_003_quorum.txt'
    </pre>
#include 'vote_003_quorum.src'



    <vstatistics />
    <p>
	Pour cette résolution générale, comme d'habitude,
#           <a href="https://vote.debian.org/~secretary/gr_resolution_process/">des statistiques</a>
            des <a href="suppl_003_stats">statistiques</a>
            sur les bulletins et les accusés de réception sont rassemblées
            périodiquement durant la période du scrutin.
            De plus, la liste des votants
            sera enregistrée. La feuille d'émargement
            sera également disponible.
            De plus, la liste des <a href="vote_003_voters.txt">votants</a>
            sera enregistrée. La <a href="vote_003_tally.txt">feuille
            de compte</a> pourra être aussi consultée.
         </p>

    <vmajorityreq />
    <p>
      Les propositions ont besoin d’une majorité qualifiée de 3 contre 1.
    </p>
#include 'vote_003_majority.src'

    <voutcome />
#include 'vote_003_results.src'

    <hrline />
      <address>
        <a href="mailto:secretary@debian.org">Secrétaire du projet Debian</a>
      </address>

