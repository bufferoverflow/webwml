#use wml::debian::translation-check translation="90225f939cba12d08795c4f97d8604b5038087ea" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Le fournisseur de service Shibboleth est vulnérable à un défaut de
déréférencement de pointeur NULL dans la fonction de récupération de
session basée sur les cookies. Un attaquant distant non authentifié peut
tirer avantage de ce défaut pour provoquer un déni de service (plantage
dans le service ou le démon shibd).</p>

<p>Pour des informations complémentaires, veuillez vous référer à l'annonce
de l'amont
<a href="https://shibboleth.net/community/advisories/secadv_20210426.txt">\
https://shibboleth.net/community/advisories/secadv_20210426.txt</a>.</p>

<p>Pour la distribution stable (Buster), ce problème a été corrigé dans la
version 3.0.4+dfsg1-1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets shibboleth-sp.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de shibboleth-sp,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/shibboleth-sp">\
https://security-tracker.debian.org/tracker/shibboleth-sp</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4905.data"
# $Id: $
