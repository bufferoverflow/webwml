#use wml::debian::translation-check translation="311fc8ec2d42b504557421d195cc5f020a977dbc" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une augmentation de droits, un déni de service ou une
fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14625">CVE-2018-14625</a>

<p>Un bogue d’utilisation de mémoire après libération a été découvert dans le
pilote vhost pour le protocole Virtual Socket. Si ce pilote est utilisé pour
communiquer avec un invité de machine virtuelle malveillant, l’invité pourrait
lire des informations sensibles du noyau de l’hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16884">CVE-2018-16884</a>

<p>Un défaut a été découvert dans l’implémentation de client NFS 4.1. Monter des
partages NFS dans plusieurs espaces de noms réseau au même moment pourrait
conduire à une utilisation après libération. Des utilisateurs locaux peuvent
être capables d’utiliser cela pour un déni de service (corruption de mémoire ou
plantage) ou éventuellement pour une augmentation de droits.</p>

<p>Cela peut être mitigé en désactivant la faculté pour des utilisateurs non
privilégiés de créer des espaces de noms, ce qui est fait par défaut dans
Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19824">CVE-2018-19824</a>

<p>Hui Peng et Mathias Payer ont découvert un bogue d’utilisation de mémoire
après libération dans le pilote audio USB. Un attaquant physiquement présent,
capable de relier un périphérique USB spécialement conçu, pourrait utiliser cela
pour une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19985">CVE-2018-19985</a>

<p>Hui Peng et Mathias Payer ont découvert une vérification manquante de limites
dans le pilote série USB hso. Un attaquant physiquement présent, capable de
relier un périphérique USB spécialement conçu, pourrait utiliser cela pour lire
des informations sensibles du noyau ou pour provoquer un déni de service
(plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20169">CVE-2018-20169</a>

<p>Hui Peng et Mathias Payer ont découvert une vérification manquante de limites
dans le cœur d’USB. Un attaquant physiquement présent, capable de relier un
périphérique USB spécialement conçu, pourrait utiliser cela pour provoquer un
déni de service (plantage) ou éventuellement pour une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000026">CVE-2018-1000026</a>

<p>Linux pourrait propager des paquets réseau regroupés avec une taille de
segmentation trop grande pour le périphérique de sortie. Dans le cas particulier
des adaptateurs 10 Gb NetXtremeII de Broadcom, cela pourrait aboutir à un déni
de service (plantage du micrologiciel). Cette mise à jour ajoute une mitigation
au pilote bnx2x pour ce matériel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3459">CVE-2019-3459</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-3460">CVE-2019-3460</a>

<p>Shlomi Oberman, Yuli Shapiro et l’équipe de recherche Karamba Security Ltd.
ont découvert une vérification manquante d’intervalle dans l’implémentation
Bluetooth L2CAP. Si Bluetooth est activé, un attaquant à proximité pourrait
utiliser cela pour lire des informations sensibles du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3701">CVE-2019-3701</a>

<p>Muyu Yu et Marcus Meissner ont signalé que l’implémentation de passerelle CAN
permettait de modifier la longueur de trame, conduisant classiquement à des
mappages en mémoire d’écritures E/S hors limites. Sur un système avec des
périphériques CAN présents, un utilisateur local avec la capacité CAP_NET_ADMIN
dans l’espace de noms net initial pourrait utiliser cela pour provoquer a
plantage (oops) ou un autre impact fonction du matériel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3819">CVE-2019-3819</a>

<p>Une boucle infinie potentielle a été découverte dans l’interface HID debugfs
exposée sous /sys/kernel/debug/hid. Un utilisateur avec accès à ces fichiers
pourrait utiliser cela pour un déni de service.</p>

<p>Cette interface est seulement accessible au superutilisateur par défaut, ce
qui limite beaucoup ce problème.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6974">CVE-2019-6974</a>

<p>Jann Horn a signalé un bogue d’utilisation de mémoire après libération dans
KVM. Un utilisateur local ayant accès à /dev/kvm pourrait utiliser cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou
éventuellement pour une augmentation de droits.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7221">CVE-2019-7221</a>

<p>Jim Mattson et Felix Wilhelm ont signalé une utilisation après libération
dans l’implémentation imbriquée de VMX dans KVM. Sur les systèmes avec des CPU
d’Intel, un utilisateur local avec accès à /dev/kvm pourrait utiliser cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou
éventuellement pour une augmentation de droits.</p>

<p>VMX imbriqué est désactivé par défaut, ce qui limite beaucoup ce problème.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7222">CVE-2019-7222</a>

<p>Felix Wilhelm a signalé une fuite d'informations dans KVM pour x86. Un
utilisateur local avec accès à /dev/kvm pourrait utiliser cela pour lire des
informations sensibles du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-8980">CVE-2019-8980</a>

<p>Un bogue a été découvert dans la fonction kernel_read_file() utilisée pour
charger des fichiers de micrologiciel. Dans certaines conditions d’erreur, il
pourrait divulguer la mémoire, ce qui pourrait conduire à un déni de service.
Cela n’est probablement pas exploitable dans un système Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9213">CVE-2019-9213</a>

<p>Jann Horn a signalé que des tâches privilégiées pourraient provoquer que
des segments de pile, y compris ceux d’autres processus, progressent vers le bas
jusqu’à l’adresse 0. Sur des systèmes sans SMAP (x86) ou PAN (ARM), cela
pourrait aggraver d’autres vulnérabilités : un déréférencement de pointeur NULL
pourrait être exploité pour une augmentation de droits plutôt que seulement un
déni de service.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 4.9.168-1~deb8u1. Cette version inclut aussi des correctifs pour les
bogues Debian n° 904385, 918103 et 922306, et
d’autres correctifs inclus dans les mises à jour de la version stable de l’amont.</p>

<p>Nous vous recommandons de mettre à niveau vos paquets linux-4.9 et
linux-latest-4.9. Vous aurez besoin d’utiliser <code>apt-get upgrade --with-new-pkgs</code>
ou <code>apt upgrade</code> car le nom des paquets binaires a changé.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify le following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1771.data"
# $Id: $
