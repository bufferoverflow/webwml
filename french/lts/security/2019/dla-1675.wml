#use wml::debian::translation-check translation="2c91f9f0e63a99464dda2301a062e96365ab008a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Alexander Kjäll et Stig Palmquist ont découvert une vulnérabilité dans
python-gnupg, une enveloppe autour de GNU Privacy Guard. Il était possible
d’injecter des données à l’aide de la propriété de phrase secrète des fonctions
gnupg.GPG.encrypt() et gnupg.GPG.decrypt() lorsque le chiffrement symétrique est
utilisé. La phrase secrète fournie n’est pas validée pour "newline" et la
bibliothèque transmet --passphrase-fd=0 à l’exécutable gpg qui attend la phrase
de passe sur la première ligne de stdin, et le texte chiffré à déchiffrer ou le
texte simple à chiffrer sur les lignes suivantes.</p>

<p>En fournissant une phrase secrète contenant un "newline", un attaquant peut
contrôler ou modifier le texte chiffré ou simple à déchiffrer ou chiffrer.</p>


<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans la version 0.3.6-1+deb8u1.

<p>Nous vous recommandons de mettre à jour vos paquets python-gnupg.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1675.data"
# $Id: $
