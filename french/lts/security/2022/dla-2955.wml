#use wml::debian::translation-check translation="f60f02c3ff3e5087c373f37280873f10dda3d4c8" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que bind9, un serveur de noms de domaine internet,
était vulnérable à un empoisonnement de cache. Lors de l'utilisation de
transitaires (« forwarders »), de faux enregistrements NS fournis par ou à
travers ces transitaires peuvent être mis en cache et utilisés par named
s'il nécessite une récursion pour une raison quelconque. Il obtient et
transmet alors des réponses éventuellement incorrectes.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
1:9.10.3.dfsg.P4-12.3+deb9u11.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bind9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de bind9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2955.data"
# $Id: $
