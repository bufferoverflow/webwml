#use wml::debian::template title="Utilizzo di git per lavorare sul sito web Debian" MAINPAGE="true"
#use wml::debian::toc
#use wml::debian::translation-check translation="7d4921b13ecaf05ffd68527f4276d1f53cad5e16" maintainer="Luca Monducci"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#work-on-repository">Utilizzo del repository Git</a></li>
<li><a href="#write-access">Accesso in scrittura al repository Git</a></li>
<li><a href="#notifications">Recezione di notifiche</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> <a href="https://git-scm.com/">Git</a>
è un <a href="https://en.wikipedia.org/wiki/Version_control">sistema
per il controllo delle versioni</a> che aiuta nella gestione di più
persone che lavorano simultaneamente sullo stesso materiale. Ogni utente
detiene una copia locale di un repository principale. Le copie locali
possono essere sulla stessa macchina oppure sparse su tutto il pianeta.
Gli utenti possono modificare la copia locale a proprio piacimento e
quando hanno completato la loro attività mandare al repository principale
le proprie modifiche.
</p>
</aside>

<h2><a id="work-on-repository">Utilizzo del repository Git</a></h2>

<p>
Iniziamo, in questa sezione imparerete come creare una copia locale
del repository principale, come mantenere tale repository aggiornato
e come inviare il tuo lavoro.
Spiegheremo anche come lavorare sulle traduzioni.
</p>

<h3><a name="get-local-repo-copy">Ottenere una copia locale</a></h3>

<p>
Come prima cosa, installare Git. Poi, configurare Git e inserire nome e
indirizzo email. Se è la prima volta che si utilizza Git, si raccomanda di
leggere la documentazione fi Git.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://git-scm.com/doc">Documentazione di Git</a></button></p>

<p>
Il passo successivo è clonare il repository (cioè creare
la copia locale). È possibile farlo in due modi:
</p>

<ul>
  <li>È necessario registrarsi su <url https://salsa.debian.org/> e
  attivare l'accesso tramite SSH abbinando una chiave pubblica
  al proprio utente salsa. Consultare le <a
  href="https://salsa.debian.org/help/ssh/README.md">pagine di aiuto di
  salsa</a> per ulteriori informazioni. Poi sarà possibile
  clonare il repository <code>webwml</code> utilizzando il comando seguente:
<pre>
   git clone git@salsa.debian.org:webmaster-team/webwml.git
</pre>
  </li>
  <li>In alternativa è possibile clonare il repository usando il protocollo
  HTTPS. In questo modo si ottiene una copia locale del repository ma non
  sarà possibile inviare (push) direttamente le modifiche:
<pre>
  git clone https://salsa.debian.org/webmaster-team/webwml.git
</pre>
  </li>
</ul>

<p>
<strong>Nota:</strong> La clonazione dell'intero repository <code>webwml</code>
richiede di scaricare circa 1,3&nbsp;GB
di dati e potrebbe essere problematica per coloro che hanno collegamenti a
Internet lenti o instabili. È possibile usare la clonazione superficiale con
una profondità iniziale minima per diminuire i dati da scaricare:
</p>

<pre>
  git clone git@salsa.debian.org:webmaster-team/webwml.git --depth 1
</pre>

<p>Una volta ottenuto un repository (superficiale) utilizzabile, è possibile
aumentare la profondità della copia locale o eventualmente convertirla in
una copia locale completa:</p>

<pre>
  git fetch --deepen=1000 # aumenta la profondità del repo di altri 1000 commit
  git fetch --unshallow   # recupera tutti i commit mancanti, converte il repo in uno completo
</pre>

<p>Inoltre è possibile fare il checkout solo di un sottoinsieme di pagine
in questo modo:</p>

<ol>
   <li><code>git clone --no-checkout git@salsa.debian.org:webmaster-team/webwml.git</code></li>
   <li><code>cd webwml</code></li>
   <li><code>git config core.sparseCheckout true</code></li>
   <li>Creare nella directory <code>webwml</code> il file <code>.git/info/sparse-checkout</code>
   per definire i contenuti di cui fare il checkout. Per esempio per recuperare i soli file di
   base, le pagine in inglese e le traduzioni in spagnolo e catalano, dovrebbe essere così:
   <pre>
      /*
      !/[a-z]*/
      /english/
      /catalan/
      /spanish/
   </pre></li>
   <li>Infine fare il checkout del repository: <code>git checkout --</code></li>
</ol>

<h3><a name="submit-changes">Inviare le modifiche locali</a></h3>

<h4><a name="keep-local-repo-up-to-date">Tenere il repository locale aggiornato</a></h4>

<p>Ogni due o tre giorni (e sicuramente sempre prima di iniziare a lavorare
sulle modifiche!) si dovrebbe eseguire</p>

<pre>
  git pull
</pre>

<p>per recuperare qualsiasi file dal repository che ha subito modifiche.</p>

<p>
È fortemente raccomandato tenere la propria directory di lavoro locale di Git
pulita prima di effettuare <code>git pull</code> e le successive modifiche. Se ci
sono delle modifiche delle quali ancora non è stato fatto il commit o dei
commit locali che non sono presenti nel branch attuale sul server remoto,
l'esecuzione di <q>git pull</q> creerà automaticamente dei merge commit oppure
potrebbe fallire a causa di conflitti. È consigliabile tenere i propri lavori
non completati in un branch differente oppure usare comandi come <q>git
stash</q>.
</p>

<p>Nota: Git è un sistema per il controllo delle versioni distribuito (non
centralizzato), ciò vuol dire che il commit delle proprie modifiche viene
memorizzato solo nella propria copia locale. Per condividere le modifiche
con gli altri è necessario eseguire anche il push delle proprie modifiche
verso il repository centrale su Salsa.</p>

<h4><a name="example-edit-english-file">Esempio di come modificare una pagina in inglese</a></h4>

<p>
Di seguito un esempio su come modificare dei file che fanno parte del codice
sorgente del sito web in inglese del. Dopo aver recuperato una copia locale
del repository con <q>git clone</q> e prima di iniziare le modifiche, eseguire
questo comando:
</p>

<ol>
  <li><code>git pull</code></li>
  <li>Adesso è possibile modificare i file.</li>
  <li>Una volta completato, eseguire il commit delle modifiche sul proprio
  repository locale con:
    <pre>
    git add percorso/al/file
    git commit -m "Messaggio di commit"
    </pre></li>
  <li>Se si dispone dell'<a href="#write-access">accesso in scrittura senza
  limitazioni</a> al repository remoto <code>webwml</code>, è anche possibile
  fare il push delle modifiche direttamente sul repository su Salsa:
  <code>git push</code></li>
  <li>Se non si dispone dell'accesso in scrittura al repository <code>webwml</code>,
  si possono inviare le proprie modifiche tramite una <a href="#write-access-via-merge-request">richiesta di unione</a>
  oppure chiedere aiuto agli altri sviluppatori.</li>
</ol>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://git-scm.com/docs/gittutorial">Documentazione di Git</a></button></p>

<h4><a name="closing-debian-bug-in-git-commits">Chiudere bug in Debian con commit su Git</a></h4>

<p>
Se nel messaggio di commento di un commit viene inserito
<code>Closes: #</code><var>nnnnnn</var> allora quando verrà fatto il 
push delle modifiche il bug con numero <code>#</code><var>nnnnnn</var>
verrà chiuso automaticamente. Il formato esatto di questa funzionalità
è lo stesso di <a href="$(DOC)/debian-policy/ch-source.html#id24">Debian
policy</a>.</p>

<h4><a name="links-using-http-https">Collegamenti tramite HTTP/HTTPS</a></h4>

<p>Molti dei siti web di Debian supportano SSL/TLS, quindi utilizzare
collegamenti HTTPS ogni volta che sia possibile e sensato. <strong>Tuttavia</strong>
alcuni siti web Debian/DebConf/SPI/ecc. non supportano HTTPS oppure usano
come Certification Autority solo SPI (e non una CA SSL fidata da tutti i
browser). Per evitare di inviare messaggi d'errore agli utenti, non
inserire collegamenti verso questi siti con HTTPS.</p>

<p>Il repository Git rigetta qualsiasi commit che contiene dei collegamenti
con HTTP a siti web Debian che supportano HTTPS o che contiene dei
collegamenti HTTPS a siti web di Debian/DebConf/SPI dei quali è noto che
non supportano HTTPS o che utilizzano certificati firmati solo da SPI.</p>

<h3><a name="translation-work">Lavorare sulle traduzioni</a></h3>

<p>Le traduzioni devono essere tenute aggiornate con il file in inglese
corrispondente. L'intestazione <code>translation-check</code> nei file delle
traduzioni è usato per tracciare su quale versione del file inglese è
basata la traduzione. Se si cambia il file tradotto, è necessario
aggiornare l'intestazione translation-check in modo che corrisponda
all'hash del commit git della corrispondente modifica sul file in inglese.
È possibile recuperare il valore dell'hash con</p>

<pre>
  git log percorso/al/file/in/inglese
</pre>

<p>Per fare una nuova traduzione, usare lo script <q>copypage.pl</q>. Creer&agrave;
un modello per la propria lingua impostando sulla traduzione la corretta
intestazione.</p>

<h4><a name="translation-smart-change">Cambiare le traduzioni con smart_change.pl</a></h4>

<p><code>smart_change.pl</code> è uno script usato per rendere più semplice
fare assieme l'aggiornamento dei file originali e delle loro traduzioni. Può
essere utilizzato in due modi, in base ai cambiamenti che si stanno
facendo.</p>

<p>
Per usare <code>smart_change</code> solo per aggiornare l'intestazione
translation-check quando si lavora sui file manualmente:
</p>

<ol>
  <li>Cambiare i file originali e fare il commit</li>
  <li>Aggiornare le traduzioni</li>
  <li>Eseguire <code>smart_change.pl -c COMMIT_HASH</code> (usare l'hash
  del commit delle modifiche al file originali). Prenderà le modifiche e
  aggiornerà le intestazioni nei file tradotti</li>
  <li>Rivedere le modifiche (per esempio con <code>git diff</code>)</li>
  <li>Fare il commit delle modifiche alle traduzioni</li>
</ol>

<p>
Oppure, usare smart_change con una espressione regolare per fare più
cambiamenti su più file in un solo passaggio:</p>

<ol>
  <li>Eseguire <code>smart_change.pl -s s/PIPPO/PLUTO/ fileoriginale1
    fileoriginale2 ...</code></li>
  <li>Rivedere le modifiche (per esempio con <code>git diff</code>)
  <li>Fare il commit dei file originali</li>
  <li>Eseguire <code>smart_change.pl fileoriginale1 fileoriginale2</code>
    (questa volta <strong>senza l'espressione regolare</strong>); questo
    aggiorna le intestazioni nei file delle traduzioni</li>
  <li>Infine, fare il commit delle traduzioni modificate</li>
</ol>

<p>
Rispetto al precedente è più complicato, richiede due commit, ma è
inevitabile a causa di come git gestisce gli hash dei commit.
</p>

<h2><a id="write-access">Accesso in scrittura al repository Git</a></h2>

<p>
L'intero codice sorgente del sito web Debian è gestito con Git, il
repository è <url https://salsa.debian.org/webmaster-team/webwml/>.
Agli ospiti non è permesso di fare il push dei propri commit nel repository
con il codice sorgente. Per ottenere l'accesso in scrittura al repository è
necessario avere un permesso particolare.
</p>

<h3><a name="write-access-unlimited">Accesso in scrittura illimitato</a></h3>
<p>
Chi necessita dell'accesso in scrittura senza limitazioni (per esempio, si
vuol diventare un autore attivo), può chiedere l'accesso in scrittura tramite
l'interfaccia web <url https://salsa.debian.org/webmaster-team/webwml/> dopo
aver fatto il login sulla piattaforma Salsa di Debian.
</p>

<p>
Se non si conosce come avviene lo sviluppo del sito web Debian e non si
hanno esperienze precedenti, si dovrebbe anche inviare una email a <a
href="mailto:debian-www@lists.debian.org">debian-www@lists.debian.org</a>
con una breve introduzione di sé stessi prima di inviare la richiesta di
accesso in scrittura. L'introduzione dovrebbe contenere informazioni
utili come in quale lingua o su quale parte del sito web si vuole lavorare
e chi può fare da garante.
</p>

<h3><a name="write-access-via-merge-request">Accesso in scrittura al repository tramite Merge Request</a></h3>
<p>
Se non si intende ottenere l'accesso in scrittura senza limitazioni oppure
non si è in grado di farlo, è comunque sempre possibile inviare una Merge
Request e lasciare che altri sviluppatori rivedano e accettino il vostro
lavoro. Inviare le proprie Merge Request usando la procedura standard della
piattaforma Salsa GitLab tramite l'interfaccia web e consultare i seguenti
documenti:
</p>

<ul>
  <li><a href="https://docs.gitlab.com/ee/user/project/repository/forking_workflow.html">Project forking workflow</a></li>
  <li><a href="https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html#when-you-work-in-a-fork">When you work in a fork</a></li>
</ul>

<p>
Le Merge Request non sono controllate da tutti gli sviluppatori del sito
web e non sempre sono lavorate in tempo. Se non si è sicuri che il vostro
contributo sia accettato, inviare una email alla lista di messaggi <a
href="https://lists.debian.org/debian-www/">debian-www</a> e chiedere la
revisione.
</p>

<h2><a id="notifications">Recezione di notifiche</a></h2>

<p>
Se si sta lavorando sul sito Debian, probabilmente si vuole sapere cosa sta
succedendo nel repository <code>webwml</code>. Ci sono due modi per rimanere
aggiornati: ricevere notifiche di commit e ricevere notifiche di richiesta
di unione.
</p>

<h3><a name="commit-notifications">Recezione di notifiche di commit</a></h3>

<p>Il progetto <code>webwml</code> su Salsa è stato configurato in modo che i commit sono
mostrati nel canale IRC #debian-www.</p>

<p>
Chi vuole, può ricevere delle notifiche via email quando avvengono dei commit
nel repository <code>webwml</code> iscrivendosi allo pseudo-pacchetto <code>www.debian.org</code>
in tracker.debian.org e attivare la parola chiave <code>vcs</code>. Questi sono i
passaggi (da seguire solo una volta):
</p>

<ol>
  <li>Aprire con un browser web <url https://tracker.debian.org/pkg/www.debian.org>.</li>
  <li>Iscriversi allo pseudo-pacchetto <code>www.debian.org</code> (ci si può
      autenticare con SSO oppure, se non si usa già tracker.debian.org per
      altri motivi, registrarsi con email e password).</li>
  <li>Andare su <url https://tracker.debian.org/accounts/subscriptions/>, poi
      su <code>modify keywords</code>, mettere la spunta su <code>vcs</code>
      (se non c'è già) e salvare.</li>
  <li>Da questo momento in poi verrà inviata una email ogni volta che qualcuno
      fa un commit nel repository di <code>webwml</code>.</li>
</ol>

<h3><a name="merge-request-notifications">Recezione di notifiche di Merge Request</a></h3>

<p>
Chi desidera ricevere delle notifiche via email quando sono inserite delle
nuove merge request per il repository <code>webwml</code> su Salsa, può
configurare le impostazioni di notifica tramite l'interfaccia web, seguendo
questi passaggi:
</p>

<ol>
  <li>Accedere con il proprio utente Salsa e andare alla pagina principale del progetto.</li>
  <li>Fare clic sull'icona con la campanella nella parte alta della pagina principale del progetto.</li>
  <li>Scegliere il livello di notifiche preferito.</li>
</ol>
