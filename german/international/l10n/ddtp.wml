#use wml::debian::template title="Übersetzung von Paketbeschreibungen &ndash; DDTP"
#use wml::debian::toc
#use wml::debian::translation-check translation="7b06966ef55556a97589982c5f8d5e1c022b075e"

<p>
Das <a href="https://ddtp.debian.org">Debian Description Translation
Project</a> (Debian-Projekt für die Übersetzung der
Paketbeschreibungen; ursprünglich umgesetzt von
<a href="mailto:Michael%20Bramer%20%3Cgrisu@debian.org%3E">Michael
Bramer</a>)
hat das Ziel, übersetzte Paketbeschreibungen und eine Infrastruktur für
die Nutzung dieser Übersetzungen bereitzustellen. Obwohl es schon seit einigen Jahren
existiert, wurde es nach einem Einbruch in einen Debian-Server
unterbrochen und hat heute &ndash; verglichen mit früher &ndash; nur
einige grundlegende Funktionen.
</p>

<p>
Das Projekt unterstützt:
</p>
<ul>
  <li>Anfordern einer aktuellen (Sid) oder älteren Paketbeschreibung.</li>
  <li>Automatisches Einfügen von schon übersetzten Absätzen von einer
      in eine andere Paketbeschreibung.</li>
  <li>Bereitstellung von <tt>Translation-*</tt>-Dateien für die
      Spiegel und APT.</li>
</ul>

<p>
Der Abschnitt Non-free des Debian-Archivs
ist im Augenblick noch nicht übersetzbar, da dort vielleicht Lizenzen
vorhanden sind, die dies verbieten, so dass dies sorgfältig
überprüft werden müsste.
</p>

<p>
Die Übersetzung von mehr als 56000 Paketbeschreibungen ist eine große
Herausforderung. Bitte helfen Sie uns, dieses Ziel zu erreichen. Sehen
Sie sich auch unsere <a href="#todo">TODO</a>-Liste für die
verbleibenden Aufgaben an.
</p>

<toc-display/>

<toc-add-entry>Schnittstellen zum DDTP</toc-add-entry>

<p>
Da alle Schnittstellen das DDTP-Backend benutzen, müssen Sie zuerst
sicherstellen, dass Ihre Sprache unterstützt wird. Dies sollte für die
meisten Sprachen der Fall sein. Wenn Ihre Sprache nicht vorhanden sein
sollte, schreiben Sie bitte an <email debian-i18n@lists.debian.org>,
damit sie hinzugefügt werden kann.
</p>

<h3 id="DDTSS">Die Web-Schnittstelle</h3>
<p>
Es gibt eine tolle
Web-Schnittstelle namens <a href="https://ddtp.debian.org/ddtss/index.cgi/xx">\
DDTSS</a>. Diese
wurde von <a href="mailto:Martijn%20van%20Oosterhout%20%3Ckleptog@gmail.com%3E">Martijn
van Oosterhout</a> geschrieben, um den Vorgang des Übersetzens und
Korrekturlesens zu vereinfachen.
</p>

<h4>Übersicht</h4>
<p>
Das System erlaubt das Hinzufügen von Übersetzungen und zusätzlich
wird das Korrekturlesen unterstützt. Es ermöglicht, für jede Sprache
festzulegen, wie viele Korrekturrunden notwendig sind, bevor die
Übersetzung zum DDTP gesendet wird. Auch ist es möglich, sich als
Benutzer anzumelden, so dass bestimmte Aktionen nur von ausgewählten angemeldeten Benutzern
vorgenommen werden können. Die Kodierung der Texte wird vom DDTSS
automatisch für Sie übernommen.
</p>

<p>
Aktuelle Grundeinstellungen:
</p>
<dl>
  <dt>Anzahl der Korrekturlesungen:</dt><dd>3</dd>
  <dt>Unterstützte Sprachen:</dt><dd>alle vom DDTP</dd>
  <dt>Benutzeranmeldung:</dt><dd>keine, ist offen für alle</dd>
</dl>

<p>
Es ist möglich, eine Wortliste für jede Sprache festzulegen. Diese wird
benutzt, um Übersetzungsvorschläge über Tooltipps anzuzeigen. Diese Liste
ist über einen Link am Ende der Seite erreichbar.
</p>

<h4>Arbeitsablauf</h4>
<p>
Das DDTSS stellte die folgenden Punkte für alle Sprachen bereit:
</p>

<h5>Anstehende Übersetzungen (<q>Pending translation</q>)</h5>
<p>
Eine Liste der anstehenden Übersetzungen. Dies sind frei auswählbare
Paketbeschreibungen, die vom Benutzer übersetzt werden können. Sie
sieht wie folgt aus:
</p>
<pre>
exim4 (priority 52)
exim4-config (priority 52)
ibrazilian (priority 47, busy)
postgresql-client (priority 47)
postgresql-contrib (priority 47)
grap (priority 45)
</pre>

<p>
Ein Sprachteam sollte versuchen, Pakete mit hoher Priorität zu
übersetzen (diese wird abhängig von der Kategorie, z.B. essential, base ...
berechnet). Die Pakete sind daher entsprechend sortiert.
</p>

<p>
Die mit <q>busy</q> markierten Beschreibungen sind schon von einem Anderen in
Bearbeitung und für höchstens 15 Minuten nicht anwählbar. Falls
keine Übersetzung in dieser Zeit eingereicht wird, sind diese wieder
frei auswählbar.
</p>

<p>
Eine Beschreibung muss komplett übersetzt sein, bevor diese angenommen
wird. Stellen Sie daher bitte sicher, das Sie den ganzen Text
übersetzen können, bevor Sie anfangen. Drücken Sie <q>Submit</q>, um
Ihre Übersetzung hinzuzufügen oder <q>Abandon</q> (Abbruch), wenn Sie den Text
nicht übersetzen wollen. Auch ist es möglich, dass Sie Glück haben und
schon eine Übersetzung einer alten englischen Version vorhanden
ist. Die Änderungen der englischen Version werden ebenfalls angezeigt.
Sie müssen diese Änderungen der englischen Version
in die Übersetzung einarbeiten. Sie können dazu die alte Übersetzung
vom unteren Teil der Seite kopieren und dann diese anpassen.
</p>

<p>
# Does not yet work as expected
Um unschöne Schwankungen der Textbreite zu verhindern, sollten Sie
kein Zeilenende (newline) von Hand eingeben, wenn diese nicht notwendig sind
(wie bei Aufzählungen). Die Zeilen werden automatisch umgebrochen.
Bedenken Sie, dass Benutzer einzelne Teile beim Korrekturlesen hinzufügen oder
entfernen könnten, was zu einer ungleichen Zeilenlänge führen kann. Eine
Korrektur dieser ungleichen Zeilenlänge macht das 
Lesen der Änderungen bei der Korrekturlesung sehr schwierig.
</p>

<p>
Es ist auch möglich, Pakete über den Namen auszuwählen. Dies ist
sinnvoll, um vergleichbare Pakete zu übersetzen, wie manpages-de,
manpages-es, etc. und alte Übersetzungen wieder benutzen zu können.
</p>

<p>
Auch schon übersetzte Pakete können so nochmal angefordert werden, um
sie zu verbessern (bitte beachten Sie, dass das aktuelle DDTP diese
Funktion z.Z. nicht richtig unterstützt, vermeiden Sie dieses daher
noch).
</p>

<h5>Anstehende Korrekturen (<q>Pending review</q>)</h5>
<p>
Eine Liste der übersetzten Beschreibungen, die noch nicht ausreichend
korrekturgelesen wurden. Diese Liste könnte wie folgt aussehen:
</p>

<pre>
 1. aspell-es (needs review, had 1)
 2. bookmarks (needs initial review)
 3. doc-linux-ja-html (needs initial review)
 4. doc-linux-ja-text (needs initial review)
 5. gnome-menus (needs initial review)
 6. geany (needs review, had 2)
 7. initramfs-tools (needs initial review)
 8. inn2 (needs initial review)
</pre>

<p>
Die folgenden Markierungen sind vorhanden:</p>
<dl>
    <dt lang="en">needs initial review:</dt>
    <dd>Die aktuelle Version dieser Übersetzung hat noch keine einzige
        Korrekturlesung durchlaufen.</dd>

    <dt lang="en">needs review:</dt>
    <dd>Die aktuelle Version dieser Übersetzung braucht noch weitere
	Korrekturlesungen, hat aber mindestens eine schon
	durchlaufen.</dd>

    <dt lang="en">reviewed:</dt>
    <dd>Diese Übersetzung wurde von einem Benutzer nicht verändert.
        Andere Benutzer müssen ihn noch Korrektur lesen.</dd>

    <dt lang="en">owner:</dt>
    <dd>Diese Übersetzung wurde übersetzt oder beim Korrekturlesen
        vom Benutzer geändert. Andere Benutzer müssen ihn noch
	Korrektur lesen.</dd>
</dl>

<p>
Falls beim Korrekturlesen Änderungen gemacht wurden, werden diese
Änderungen farblich hervorgehoben, sobald Sie das Paket auswählen.
</p>

<h5>Kürzlich übersetzt (<q>Recently translated</q>)</h5>
<p>
Eine Liste der an das DDTP übertragenden Übersetzungen. Maximal die letzten 20
Pakete zusammen mit dem Datum der Übertragung werden aufgeführt.
</p>

<h3 id="Pootle">Die Internationalisierungsschnittstelle</h3>
<p>
Es existierten Pläne, um ein neues Rahmenwerk zur Übersetzung
verschiedener Dokumente in Debian aufzubauen, wie PO-Dateien und
Debconf-Vorlagen. Dieses sollte einmal auch Paketbeschreibungen unterstützen.
Falls dies realisiert wird und wie vorgesehen läuft, werden das aktuelle DDTP und seine
Oberfläche abgeschaltet.
</p>

<p>
Dieses Rahmenwerk würde auf
<a href="http://pootle.locamotion.org/">Pootle</a> aufbauen.
Es war ein <q>Google Summer of Code</q>
<a href="https://wiki.debian.org/SummerOfCode2006?#Translation_Coordination_System">Projekt</a>.
</p>

<toc-add-entry name="rules">Allgemeine Übersetzungsregeln</toc-add-entry>
<p>
Es ist wichtig, dass Sie keine englischen Beschreibungen während der
Übersetzung verändern. Falls Sie einen Fehler darin finden, sollten
Sie einen Fehlerbericht zu diesem Paket schreiben; Einzelheiten finden Sie unter
<a href="$(HOME)/Bugs/Reporting">Wie werden Fehler in Debian berichtet?</a>.
</p>

<p>
Übersetzen Sie die unübersetzten Teile von jedem Anhang, welche mit
&lt;trans&gt; markiert sind. Es ist wichtig, dass Sie keine Zeilen
ändern, die nur einen einzelnen Punkt am Anfang enthalten. Dies sind
Absatztrenner, die später von APT-Oberflächen nicht mit angezeigt werden.
</p>

<p>
Abschnitte, die schon übersetzt sind, wurden von anderen Übersetzungen
oder von älteren Übersetzungen übernommen (dies bedeutet, dass der
ursprüngliche englische Absatz seit dieser Zeit nicht verändert wurde).
Falls Sie solch einen Absatz ändern sollten, werden
andere Übersetzungen mit dem gleichen Abschnitt dadurch nicht verändert.
</p>

<p>
Beachten Sie auch, dass jedes Sprachteam seine eigenen Regeln, wie
Wortlisten oder zu verwendende Anführungszeichen, hat. Bitte beachten Sie diese so gut wie
möglich. Die wichtigsten Regeln für Deutsch finden Sie
<a href="../German/rules">hier</a>. Es wird
empfohlen, dass Sie zuerst fertige Übersetzungen Korrektur lesen,
entweder über <a href="#DDTSS">DDTSS</a> oder mittels Paketverwaltungsprogrammen
wie <a href="https://packages.debian.org/aptitude">aptitude</a>. Damit
bekommen Sie ein Gefühl für die Regeln des Sprachteams. Falls Sie unsicher
sind, <a href="mailto:debian-l10n-german@lists.debian.org">fragen Sie</a> das
entsprechende Sprachteam.
</p>

<toc-add-entry>Korrekturlesen und Fehlerkorrekturen</toc-add-entry>
# General proofread suggestions, not DDTSS specific
<p>
Nur das DDTSS hat zurzeit die Möglichkeit zur Korrekturlesung und
sendet Übersetzungen nur dann zum DDTP, wenn dies erledigt wurde.
</p>

<p>
Wenn Ihnen typische Fehler (z.B. Kodierungsfehler) oder andere
einfach zu korrigierende Fehler auffallen,
können diese - ohne erneute Korrekturlesung - durch ein
Skript für alle Übersetzungen geändert werden. Diese Aktion sollte
nur ein vertrauenswürdiger Koordinator durchführen, der dafür die
entsprechenden Skripte anwendet.
</p>

<p>
Da das Korrekturlesen eine längere Zeit dauern kann (vor allem bei
dauernden kleinen Änderungen), kann es eine Option sein, diese kleinen
Rechtschreibfehler und Inkonsistenzen zu ignorieren und diese (hoffentlich
gesammelten) Fehler später für alle Übersetzungen per Skript ändern
zu lassen. Dies sollte das Korrekturlesen beschleunigen und die
Fehler für alle Übersetzungen beheben. Zum Notieren typischer Fehler kann
die <a href="https://wiki.debian.org/DDTP_Korrekturen">Wiki-Seite</a> dienen.
Sie wird von Zeit zu Zeit abgearbeitet.
</p>

<toc-add-entry>Benutzen der übersetzten Paketbeschreibungen</toc-add-entry>
<p>
Korrekte Unterstützung für Übersetzungen der Paketbeschreibungen ist in APT
seit der Paketversion in <a
href="https://packages.debian.org/lenny/admin/apt">Lenny</a> enthalten. Wenn
solch eine Version benutzt wird, hat jeder Benutzer mit Programmen, die APT benutzen,
Zugriff auf die
Übersetzungen. Hierzu gehören <tt>apt-cache</tt>, <tt>aptitude</tt>,
<tt>synaptic</tt> und verschiedene andere.
</p>

<p>
APT lädt Dateien mit Namen <tt>Translation-<var>Sprache</var></tt>
von den Debian-Spiegeln herunter. Diese sind nur für Lenny und neuere Distributionen verfügbar.
Der Ort für diese Dateien auf den Spiegeln ist
<a href="http://ftp.de.debian.org/debian/dists/sid/main/i18n/">dists/main/sid/i18n/</a>.
</p>

<p>
Es ist auch möglich, die Verwendung von Übersetzungen zu deaktivieren. Um
dies zu erreichen, ist
</p>
<pre>
APT::Acquire::Translation "none";
</pre>
<p>
zu <tt>/etc/apt/apt.conf</tt> hinzuzufügen. Statt <tt>none</tt> wird auch
ein Sprachcode unterstützt.
</p>

<!--
<p>
FIXME: I need to test this script from me again against the new Translation-<lang>
files. Ignore this for now:<br />
There is also a small script available which just replaces English descriptions
in the local <tt>Packages</tt> files (<tt>/var/lib/apt/lists/</tt>) with
translations. This could be used if the new APT version cannot be installed for
whatever reasons.
</p>
-->

<toc-add-entry name="todo">TODO</toc-add-entry>

Auch wenn es einige Fortschritte beim DDTP gibt, gibt es noch viel zu tun:
<ul>
  <li>Alle Übersetzerteams suchen neue Übersetzer und Korrekturleser,
      die bei der langen Liste der Pakete helfen können.</li>
  <li>Versuchen Sie, während der Übersetzung/Korrekturlesung auch die 
      englischen Texte zu verbessern. Man könnte das vielleicht vereinfachen,
      indem man eine neue englische Pseudo-Sprache anlegt, die die verbesserten
      Beschreibungen als Übersetzung enthält, und diese nach erfolgreicher
      Korrekturlesung automatisch als Fehlerbericht versenden.</li>
  <li>Das neue Internationalisierungs-Rahmenwerk mit Pootle benötigt
      noch viel Arbeit, bis es alle unsere Vorstellungen erfüllt.</li>
</ul>
