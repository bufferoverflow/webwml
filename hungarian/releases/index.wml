#use wml::debian::template title="Debian-verziók"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="ccf6e79eefe5b0ce09c41d5b657645e86214a9f3" maintainer="Szabolcs Siebenhofer"
# translated by Viktor Nagy <chaotix@freemail.hu>
# updated by Hajnalka Hegedűs <heha@inf.elte.hu>
# updated by Szabolcs Siebenhofer <the7up@gmail.com>

<p>A Debian mindig legalább három aktív verziót
tart fenn: <q>stable</q>-t (stabilt), <q>testing</q>-et (teszt verziót) és
<q>unstable</q>-t (instabilt).

<dl>
<dt><a href="stable/">stable</a></dt>
<dd>
<p>
A stabil (<q>stable</q>) disztribúció a legfrissebb hivatalosan kiadott
Debian-disztribúciót tartalmazza. 
</p>
<p>
Ez a Debian <q>hivatalos</q> kiadása, elsősorban ennek használatát ajánljuk.
</p>
<p>
A Debian jelenlegi <q>stable</q> verziója a
<:=substr '<current_initial_release>', 0, 2:>, kódneve <em><current_release_name></em>.

<ifeq "<current_initial_release>" "<current_release>"
"A megjelenés dátuma: <current_release_date>."
/>
<ifneq "<current_initial_release>" "<current_release>"
 "Az eredeti (<current_initial_release>) megjelenésének 
 ideje <current_initial_release_date>, amelynek frissítése a
 <current_release> verzió, amely megjelenésének ideje: 
 <current_release_date>."
 />


</p>
</dd>
<dt><a href="testing/">testing</a></dt>
<dd>
<p>
  A <q>testing</q> disztribúció azokat a csomagokat tartalmazza, amelyek
  még nem kerültek be a <q>stable</q> kiadásba, de már az erre váró
  csomagok között vannak. Ennek a disztribúciónak az az előnye, hogy
  frissebb szoftvereket tartalmaz.
</p> 
<p>
  Ha többet akarsz megtudni arról, hogy <a href="$(DOC)/manuals/debian-faq/ftparchives#testing">
  mit jelent a <q>testing</q></a> és hogy <a href="$(DOC)/manuals/debian-faq/ftparchives#frozen">
  hogyan válik <q>stable</q>-lé</a>, akkor olvasd el a <a href="$(DOC)/manuals/debian-faq/">
  Debian FAQ</a>-t.
  </p>
  <p>
  A jelenlegi <q>testing</q> disztribúció neve <em><current_testing_name></em>.
</p>
</dd>

<dt><a href="unstable/">unstable</a></dt>
<dd>
<p>
Az instabil disztribúcióban történik a Debian aktív fejlesztése.
Ezt a disztribúciót általában fejlesztők futtatják, illetve azok, akik
szeretnek borotvaélen táncolni. Azoknak a felhasználóknak, akik unstable disztribúciót
használnak, javasolt feliratkozni a debian-devel-announce levelező listára,
hogy értesítést kaphassanak a jelentősebb változásokról, például frissítésekről, 
amik esetleg elbukhatnak.
<p>
Az instabil disztribúció neve <em>sid</em>.
</dd>
</dl>

<h2>Kiadások életciklusa</h2>
<p>
A Debian az új stable kiadásokat rendszeresen bejelenti. A felhasználók minden kiadásánál
3 év támogatásra számíthatnak és 2 év extra LTS támogatásra.
</p>

<p>
Nézd meg a <a href="https://wiki.debian.org/DebianReleases">Debian kiadások</a>
Wiki oldalt és a <a href="https://wiki.debian.org/LTS">Debian LTS</a>
Wiki oldalt a részletes információkért.
</p>

<h2>A kiadások jegyzéke</h2> 

<ul>

  <li><a href="<current_testing_name>/">A Debian következő verziója a 
    <q><current_testing_name></q> nevet kapta</a>
      &mdash; a kiadás dátuma még nem ismert
  </li>

  <li><a href="buster/">Debian 10 (<q>buster</q>)</a>
  &mdash; jelenlegi stabil verzió
</li>

  <li><a href="stretch/">Debian 9 (<q>stretch</q>)</a>
    &mdash; elavult stabil verzió, <a href="https://wiki.debian.org/LTS">LTS támogatás</a>
    alatt.
  </li>
  <li><a href="jessie/">Debian 8 (<q>jessie</q>)</a>
    &mdash; elavult stabil verzió, <a href="https://wiki.debian.org/LTS/Extended">kitejesztett 
    LTS támogatás</a> alatt.
  </li>
  <li><a href="wheezy/">Debian 7 (<q>wheezy</q>)</a>
      &mdash; elavult stabil verzió
  </li>
  <li><a href="squeeze/">Debian 6.0 (<q>squeeze</q>)</a>
      &mdash; elavult stabil verzió
  </li>
  <li><a href="lenny/">Debian GNU/Linux 5.0 (<q>lenny</q>)</a>
      &mdash; elavult stabil verzió
  </li>
  <li><a href="etch/">Debian GNU/Linux 4.0 (<q>etch</q>)</a>
      &mdash; elavult stabil verzió
  </li>
  <li><a href="sarge/">Debian GNU/Linux 3.1 (<q>sarge</q>)</a>
      &mdash; elavult stabil verzió
  </li>
  <li><a href="woody/">Debian GNU/Linux 3.0 (<q>woody</q>)</a>
      &mdash; elavult stabil verzió
  </li>
  <li><a href="potato/">Debian GNU/Linux 2.2 (<q>potato</q>)</a>
      &mdash; elavult stabil verzió
  </li>
  <li><a href="slink/">Debian GNU/Linux 2.1 (<q>slink</q>)</a> 
      &mdash; elavult stabil verzió
  </li>
  <li><a href="hamm/">Debian GNU/Linux 2.0 (<q>hamm</q>)</a>
      &mdash; elavult stabil verzió
  </li>
</ul>

<p>Az elavult Debian-kiadások weboldalait meghagyjuk, de maguk a kiadások 
csak az <a href="$(HOME)/distrib/archive"> archívumban</a> 
találhatóak meg.</p>

<p>Ha kíváncsi vagy arra, hogy 
<a href="$(HOME)/doc/manuals/debian-faq/ftparchives#sourceforcodenames">honnan származnak a verziók kódnevei</a>, olvasd el a <a href="$(HOME)/doc/manuals/debian-faq/">Debian FAQ</a>-t.</p>


 
<h2>A kiadásokban lévő adatok sértetlensége</h2>
   
<p>Az adatok sértetlenségét egy digitális aláírással ellátott <code>Release</code>
fájl garantálja. Hogy megbizonyosodhass arról, hogy minden fájl, ami a kiadásban 
van, tényleg oda tartozik, az összes csomagfájl ellenőrző összegét bemásoltuk
a <code>Release</code> fájlba.</p>
   
<p>Az ehhez tartozó digitális aláírások a <code>Release.gpg</code>
fájlban találhatók, amelyek az archívum jelenlegi kulcsával készültek.
A <q>stabil</q> és a <q>korábbi</q> (oldstable) kiadások számára egy további, kapcsolat
nélküli használathoz való kulcsot bocsátott ki a
<a href="$(HOME)/intro/organization#release-team">Stabil kiadásért felelős csapat</a>
egy tagja.</p>
