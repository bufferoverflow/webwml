<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A global out of bounds read when encoding gif from malformed input was
found in this software.</p>

<p>When given invalid inputs, we might be fed the EOF marker before it is
actually the EOF.  The gif logic assumes once it sees the EOF marker,
there won't be any more data, so it leaves the cur_bits index possibly
negative.  So when we get more data, we underflow the masks array.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.0.36~rc1~dfsg-6.1+deb7u5.</p>

<p>We recommend that you upgrade your libgd2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-563.data"
# $Id: $
