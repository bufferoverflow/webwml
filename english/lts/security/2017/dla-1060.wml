<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-0663">CVE-2017-0663</a>

    <p>Invalid casting of different structs could enable an attacker to
    remotely execute some code within the context of an unprivileged
    process.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7376">CVE-2017-7376</a>

    <p>Incorrect limit used for port values.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.8.0+dfsg1-7+wheezy9.</p>

<p>We recommend that you upgrade your libxml2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1060.data"
# $Id: $
