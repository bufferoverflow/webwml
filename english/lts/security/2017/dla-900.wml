<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The Freetype 2 font engine was vulnerable to an out-of-bounds write
caused by a heap-based buffer overflow in the cff_parser_run function
in cff/cffparse.c.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2.4.9-1.1+deb7u5.</p>

<p>We recommend that you upgrade your freetype packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-900.data"
# $Id: $
