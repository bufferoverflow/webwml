<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Some vulnerabilities have been found in ClamAV, an open source antivirus
engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-0202">CVE-2018-0202</a>

    <p>It was found that ClamAV didn't process certain PDF files correctly,
    relating to a heap overflow. Specially crafted PDFs could yield ClamAV
    to crash, resulting in a denial-of-service or potentially execution of
    arbitrary code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1000085">CVE-2018-1000085</a>

    <p>Hanno B�ck discovered that ClamAV didn't process XAR files correctly.
    Malformed XAR files could cause ClamAV to crash by an out of bounds
    heap read. This could result in a denial-of-service.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.99.4+dfsg-1+deb7u1.</p>

<p>We recommend that you upgrade your clamav packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1307.data"
# $Id: $
