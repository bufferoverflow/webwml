<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues were discovered in QEMU, a fast processor emulator, which
could result in denial of service, information disclosure or the the
execution of arbitrary code.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
1:2.8+dfsg-6+deb9u17.</p>

<p>We recommend that you upgrade your qemu packages.</p>

<p>For the detailed security status of qemu please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/qemu">https://security-tracker.debian.org/tracker/qemu</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2970.data"
# $Id: $
