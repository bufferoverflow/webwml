<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was potential SQL injection attack
vulnerability in Django, a popular Python-based web development framework:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28346">CVE-2022-28346</a>

	<p> CVE-2022-28346: An issue was discovered in Django 2.2 before
	2.2.28, 3.2 before 3.2.13, and 4.0 before 4.0.4. QuerySet.annotate(),
	aggregate(), and extra() methods are subject to SQL injection in column
	aliases via a crafted dictionary (with dictionary expansion) as the
	passed **kwargs.</p></li>

</ul>

<p>For Debian 9 <q>Stretch</q>, this problem have been fixed in version
1:1.10.7-2+deb9u16.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2982.data"
# $Id: $
