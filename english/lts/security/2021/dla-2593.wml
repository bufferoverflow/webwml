<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>This update reverts the Symantec CA blacklist (which was originally
#911289). The following root certificates were added back (+):
  + <q>GeoTrust Global CA</q>
  + <q>GeoTrust Primary Certification Authority</q>
  + "GeoTrust Primary Certification Authority - G2"
  + "GeoTrust Primary Certification Authority - G3"
  + <q>GeoTrust Universal CA</q>
  + <q>thawte Primary Root CA</q>
  + "thawte Primary Root CA - G2"
  + "thawte Primary Root CA - G3"
  + "VeriSign Class 3 Public Primary Certification Authority - G4"
  + "VeriSign Class 3 Public Primary Certification Authority - G5"
  + <q>VeriSign Universal Root Certification Authority</q>
<p><b>Note</b>: due to bug #743339, CA certificates added back in this version
won't automatically be trusted again on upgrade.  Affected users may
need to reconfigure the package to restore the desired state.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
20200601~deb9u2.</p>

<p>We recommend that you upgrade your ca-certificates packages.</p>

<p>For the detailed security status of ca-certificates please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/ca-certificates">https://security-tracker.debian.org/tracker/ca-certificates</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2593.data"
# $Id: $
