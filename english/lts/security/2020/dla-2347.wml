<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several minor vulnerabilities have been discovered in libvncserver, a
server and client implementation of the VNC protocol.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20839">CVE-2019-20839</a>

    <p>libvncclient/sockets.c in LibVNCServer had a buffer overflow via a
    long socket filename.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14397">CVE-2020-14397</a>

    <p>libvncserver/rfbregion.c has a NULL pointer dereference.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14399">CVE-2020-14399</a>

    <p>Byte-aligned data was accessed through uint32_t pointers in
    libvncclient/rfbproto.c.</p>

    <p>NOTE: This issue has been disputed by third parties; there is
    reportedly <q>no trust boundary crossed</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14400">CVE-2020-14400</a>

    <p>Byte-aligned data was accessed through uint16_t pointers in
    libvncserver/translate.c.</p>

    <p>NOTE: This issue has been disputed by third parties. There is no
    known path of exploitation or cross of a trust boundary.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14401">CVE-2020-14401</a>

    <p>libvncserver/scale.c had a pixel_value integer overflow.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14402">CVE-2020-14402</a>

    <p>libvncserver/corre.c allowed out-of-bounds access via encodings.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14403">CVE-2020-14403</a>

    <p>libvncserver/hextile.c allowed out-of-bounds access via encodings.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14404">CVE-2020-14404</a>

    <p>libvncserver/rre.c allowed out-of-bounds access via encodings.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-14405">CVE-2020-14405</a>

    <p>libvncclient/rfbproto.c did not limit TextChat size.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
0.9.11+dfsg-1.3~deb9u5.</p>

<p>We recommend that you upgrade your libvncserver packages.</p>

<p>For the detailed security status of libvncserver please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libvncserver">https://security-tracker.debian.org/tracker/libvncserver</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2347.data"
# $Id: $
