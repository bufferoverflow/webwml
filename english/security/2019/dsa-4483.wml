<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Two security issues have been discovered in LibreOffice:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9848">CVE-2019-9848</a>

    <p>Nils Emmerich discovered that malicious documents could execute
    arbitrary Python code via LibreLogo.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9849">CVE-2019-9849</a>

    <p>Matei Badanoiu discovered that the stealth mode did not apply to
    bullet graphics.</p></li>

</ul>

<p>For the oldstable distribution (stretch), these problems have been fixed
in version 1:5.2.7-1+deb9u9.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 1:6.1.5-3+deb10u2.</p>

<p>We recommend that you upgrade your libreoffice packages.</p>

<p>For the detailed security status of libreoffice please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libreoffice">\
https://security-tracker.debian.org/tracker/libreoffice</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4483.data"
# $Id: $
